FROM locustio/locust

COPY locust/templates/index.html templates/index.html
COPY locust/templates/index.html /opt/venv/lib/python3.9/site-packages/locust/templates/index.html
RUN find / | grep index.html

