#!/bin/env python3
from locust import HttpUser, between, task

credentials={
    "email": "locust@octomy.org",
    "password": "123Locust!"
}

"""
class OverwatchLandingUser(HttpUser):
    @task
    def login_page(self):
        self.client.get("/overwatch/access/login")

    @task
    def landing_page(self):
        self.client.get("/")
#"""

"""
class NginxStaticUser(HttpUser):
    @task
    def nginx(self):
        self.client.get("/triage/nginx")
#"""


"""
class TriageUser(HttpUser):
    
    @task(30)
    def nginx_up(self):
        self.client.get("/triage/nginx")

    @task(30)
    def flask_up(self):
        self.client.get("/triage/flask")

    @task(30)
    def postgres_up(self):
        self.client.get("/triage/postgres")

    @task(10)
    def flask_static(self):
        self.client.get("/static/shopify_static.txt")


    @task(1)
    def http_200(self):
        self.client.get("/triage/error/200")

    @task(1)
    def http_400(self):
        self.client.get("/triage/error/400")
    
    @task(1)
    def http_401(self):
        self.client.get("/triage/error/401")
    
    @task(1)
    def http_403(self):
        self.client.get("/triage/error/403")
    
    @task(1)
    def http_404(self):
        self.client.get("/triage/error/404")

    @task(1)
    def http_418(self):
        self.client.get("/triage/error/418")

    @task(1)
    def http_500(self):
        self.client.get("/triage/error/500")

    @task(1)
    def http_502(self):
        self.client.get("/triage/error/502")


#"""


"""
class OverwatchUser(HttpUser):
    def on_start(self):
        self.client.post("/overwatch/access/login", json=credentials)

    @task
    def stores(self):
        self.client.get("/shopify/stores", headers={"accept": "application/json"})
    #
#"""

#"""
class OverwatchPagesUser(HttpUser):
    def on_start(self):
        self.client.post("/overwatch/access/login", json=credentials)
#
    @task
    def status(self):
        self.client.get("/shopify/status")

    @task
    def stores(self):
        self.client.get("/shopify/stores")

    @task
    def templates(self):
        self.client.get("/shopify/popup_engine/templates")

    @task
    def email(self):
        self.client.get("/shopify/email")

    @task
    def batch(self):
        self.client.get("/shopify/batch")

    @task
    def clockwork(self):
        self.client.get("/shopify/clockwork")

    @task
    def link(self):
        self.client.get("/shopify/link")

    @task
    def db2html(self):
        self.client.get("/shopify/db2html")

    @task
    def eval(self):
        self.client.get("/shopify/eval")

    @task
    def routes(self):
        self.client.get("/shopify/routes")

    @task
    def environment(self):
        self.client.get("/shopify/environment")

    @task
    def data(self):
        self.client.get("/shopify/data")

    @task
    def access(self):
        self.client.get("/shopify/access")

    @task
    def routes(self):
        self.client.get("/shopify/routes")

#"""


"""
class AdminUser(HttpUser):
    
    def on_start(self):
        self.client.post("/login", json=credentials)

    def get_overwatch_head(self):
        # fmt:off
        head=[
              '/static/css/app_admin.css'
            , '/static/css/app_main.css'
            , '/static/css/chart_engine.css'
            , '/static/css/hue_slider.css'
            , '/static/css/shopify_theme.css'
            , '/static/css/slide_switch.css'
            , '/static/css/slider.css'
            , '/static/css/spinner.css'
            , '/static/css/tabs.css'
            , '/static/css/overwatch.css'
            , '/static/images/merchbot_favicon_admin.png'
            #
            , '/static/images/merchbot_avatar_admin.png'
            , '/static/svg/merchbot_mascot_badge.svg'
            , '/static/js/overwatch.js'
            , '/static/js/fk.js'
            
        ]
        # fmt:on
        for item in head:
            self.client.get(item)

    @task
    def index(self):
        self.client.get("/shopify")
        self.get_overwatch_head()

    @task
    def install_start(self):
        self.client.get("/shopify/install/start")

    @task
    def install_finish(self):
        self.client.get("/shopify/install/finish")

    @task
    def kooltool_cusomers(self):
        self.client.get("/shopify/kooltool/customers")

    @task
    def kooltool_orders(self):
        self.client.get("/shopify/kooltool/orders")


#"""


"""
class StorefrontUser(HttpUser):
    host = "https://test.merchbot.net/shopify/mb"
    shop = "merchbot-test.myshopify.com"
    wait_time = between(5, 20)
    
    def on_start(self):
        pass

    @task(95)
    def buffet(self):
        self.client.get("/shopify/script_tag?shop={shop}")
        self.client.get("/shopify/mb/buffet")
        
    @task(5)
    def buffet_no_shop(self):
        self.client.get("/shopify/script_tag")
        self.client.get("/shopify/mb/buffet")
#"""


